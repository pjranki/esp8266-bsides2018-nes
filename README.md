BSides Entertainment System (2018)
==================================
NES emulator running on the 2018 BSides Canberra Badge.

This was achieved by using the 2017 badge screen and the exact same wiring.

Required Libraries
------------------
crc32fast
nes
SPI
Adafruit_ST7735

Games
-----
You need to use your own games, the nes library has a script to convert them to C headers.
